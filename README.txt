#docker-pring-boot
 - aplicacion de prueba con java y springboot
 - dentro de las properties del proyecto tiene configurado el mismo puerto que en el docker file ( 8085 )
 - se puede correr desde un ide o desde DOCKER container
 - hacer un mvn clean y mvn install(por consola parado a la par del pom)  o desde el mismo run mvn del ide
 - si se quiere correr con un contenedor docker:
  1- una vez clonado este repo y teniendo instalado docker, entrar por cmd dentro del proyecto y al estar a la par del Dockerfile ejecutar los comando para crear la imagen y luego para crear y correr un contenedor
  2- en un browser escucha en el puerto:8085
  3-localhost:8085/rest/docker/hello  
  Tendremos la app de prueba corriendo desde un contenedor docker
  Se podra hacer export e import de este container
 -------------------------------------------------------------------------
