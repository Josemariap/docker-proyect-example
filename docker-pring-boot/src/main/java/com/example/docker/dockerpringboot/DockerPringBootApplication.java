package com.example.docker.dockerpringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController()
public class DockerPringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerPringBootApplication.class, args);
	}
	
	@RequestMapping("/rest/docker/hello")
	public String Hello(){
		return "Hello sprinBoot in docker";
	}
}
