package com.example.docker.dockerpringboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DockerPringBootApplicationTests {

	@Test
	public void contextLoads() {
	}

}
